# Szacuj-Miche!
This app enables user to compute how many calories meal has (development in progress).

## Installation
1. Clone the project `https://gitlab.com/jakub_wlodarczyk/szacuj-miche.git`.
2. Checkout on develop branch -> `git checkout develop`.
3. This project uses MySQL so you will need to run it locally and:
    - szacuj-miche/endpoints/database/connection.js - provide your credentials
    - szacuj-miche/endpoints/database/tables.sql -> create database (name: szacuj_miche) with these tables.
4. Enter `szacuj-miche` directory -> `npm i`.
5. Enter `endpoints` directory ->`npm i`.

## Usage
1. Enter `szacuj-miche` directory -> `npm start`.
2. Enter `endpoints` directory -> `npm start`.
3. Go to `http://localhost:8080`, click on `Dodaj posiłek`tab.
4. Enter a data about meal which you want compute caloricity for (enter the meal name, choose select option with category).
5. Click on `Dodaj składnik` button and enter amount of nutrients which consists of your meal.
6. If you have at least two ingredients, you can save the meal into database - click on `Wyślij` button.

## Other
Whole project is on the `develop` branch.

## Authors
- Jakub Włodarczyk - Master, Full Stack Developer, originator of the Szacuj-miche project
- Beata Kosek - Front-end Developer